+++
date = "2018-08-26T11:59:12+00:00"
description = "Today is the day this blog comes to see the light of day!"
draft = true
tags = ["meta"]
title = "Hello world!"
title_image = ""

+++
I am pleased to show this site to the world. It's been in the making for a while now and I was finally able to connect all ends to make it all work. 

So, what is this site all about, you may ask. I want to dedicate this site to sharing this and that of my greatest hobby: Making things! I like making many things, which includes but is not limited to cooking, baking and brewing but also (web-) development and other tech-heavy topics. In fact, my first (real) blog post is going to be about this very site. I will explain in all detail how I set everything up, starting with hardware exploring the corners of deploying websites with Docker and wrapping up with networking. 

If, however, that sounds way too geeky and boring to you, do not dispare! I will also soon write about how I make fantastic pizza, crunchy sour dough bread and other, more practical, recipies.

The theme of this site will (probably) be updated several times within the upcoming weeks, so don't yet fall in love with it!